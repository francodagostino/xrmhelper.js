# README #


## xrmHelper.js ##

A useful tool for Dynamics CRM forms.


## Setup ##

You just have to:

* Add the `xrmHelper.js` to the form js libraries
* Use the sample js template for the incident form: `myxrm.incident.js`, you don't need to define an onLoad function.
   

Look at sample myxrm.incident.js template to use some cool functions but keep in mind that in ribbon function you cannot use form helpers.

```javascript
// handlers
form.onLoad = function (args) {};
form.onLoadCreate = function () {};
form.onLoadUpdate = function () {};
form.onChange("field_name", form.myPublicFunc);
form.onChange("modifiedon", form.myPublicFunc);

// get/set field value
form.getValue("field_name");
form.getLookupNameValue("field_name");
form.getLookupIdValue("field_name");            
form.setValue("field_name", value); 

// show/hide field
form.show("field_name");
form.hide("field_name");
form.setVisible("field_name", false);

// enable/disable field
form.disble("field_name");
form.enable("field_name");
form.setEnabled("field_name", true);
form.setDisabled("field_name", true);

// set field as  required/not Required
form.setRequired("field_name");
form.setNotRequired("field_name");    

// check form type
form.isCreate();
form.isUpdate();
form.isReadOnly();
form.isQuickCreate();    

// others
form.getEntityId();
form.isDirty("field_name");
xrmHelper.userHasRole("role_name");
form.preventAutoSave();
```
