/** 
 * Xrm Helper
 * 
 * @module xrmHelper
 * @exports xrmHelper
 */ 
var xrmHelper = (function () {
    var xrmHelper = {};
    
    /** Get global context */
    xrmHelper.context = (function () {
        if (typeof GetGlobalContext == "function")
            return GetGlobalContext();         
        else if (typeof Xrm.Page.context == "object") {
            return Xrm.Page.context;
        } else {
            throw new Error("Unable to access Organisation name");
        }        
    })();

    /** getOdataEndpoint */
    xrmHelper.getOdataEndpoint = function () {
        return Xrm.Page.context.getClientUrl() + "/XRMServices/2011/OrganizationData.svc";
    };

    /** escapeHtml */
    xrmHelper.escapeHtml = function (text) {
        return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    };


    /** unescapeHtml */
    xrmHelper.unescapeHtml = function (text) {
        return text
            .replace(/&amp;/g, '&')
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    };


    /** encodeObjectToUri */
    xrmHelper.encodeObjectToUri = function (obj) {        
        var parts = [];
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
            }
        }
        return parts.join("&");
    };


    /** Parse querystring */
    xrmHelper.getQeryStringArray = function (url) {
        if (!url) url = window.location.href;
        var qs = {};  
        url.replace(
              new RegExp("([^?=&]+)(=([^&]*))?", "g"),
              function($0, $1, $2, $3) {                 
                  var val = decodeURIComponent(String($3).replace(/\+/g, " "));                 
                  if (qs[$1])
                      Array.isArray(qs[$1]) ? qs[$1].push(val) : qs[$1] = [qs[$1], val];          
                  else
                      qs[$1] = val; 
              }
          )       
        return qs;
    };

    /** Get role name based on id */
    xrmHelper.getRoleName = function (userRoleId) {
        var selectQuery = "RoleSet?$top=1&$filter=RoleId eq guid'" + userRoleId + "'&$select=Name";
        var odataSelect = xrmHelper.getOdataEndpoint() + "/" + selectQuery;       
        var roleName = null;
        $.ajax({
            type: "GET",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: odataSelect,
            beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
            success: function (data, textStatus, XmlHttpRequest) {
                var result = data.d;
                if (!!result) {
                    if (result.results[0] != null )
                    roleName = result.results[0].Name;
                }
            },
            error: function (XmlHttpRequest, textStatus, errorThrown) {
                console.warn('OData Select Failed: ' + odataSelect);
            }
        });
        return roleName;
    };


    /** Get role name based on id */
    xrmHelper.getRoleId = function (userRoleName) {
        var selectQuery = "RoleSet?$top=1&$filter=Name eq '" + userRoleName + "'&$select=RoleId";
        var odataSelect = xrmHelper.getOdataEndpoint() + "/" + selectQuery;
        var roleName = null;
        $.ajax({
            type: "GET",
            async: false,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: odataSelect,
            beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
            success: function (data, textStatus, XmlHttpRequest) {
                var result = data.d;
                if (!!result) {
                    if (result.results[0] != null )
                        roleName = result.results[0].RoleId;
                }
            },
            error: function (XmlHttpRequest, textStatus, errorThrown) {
                console.warn('OData Select Failed: ' + odataSelect);
            }
        });
        return roleName;
    };

    /** Check if user has role */
    xrmHelper.userHasRole = function (roleName) {        
        var searchRoleId = xrmHelper.getRoleId(roleName); 
        var currentUserRoles = Xrm.Page.context.getUserRoles();
        for (var i = 0; i < currentUserRoles.length; i++) {
            var userRoleId = currentUserRoles[i];            
            if (userRoleId == searchRoleId) {
                return true;
            }
        }
        return false;        
    };


    /**
     * SOAP functions
     */
    xrmHelper.soap = (function () {
        var soap = {};

        /** getWebUrl */
        soap.getWebUrl = function () {
          return  Xrm.Page.context.getClientUrl() + "/XRMServices/2011/Organization.svc/web";
        };

        /** getActionException */
        soap.getActionException = function (request) {
            var xmlResponse = request.responseXML.documentElement;
            var message = xmlResponse.getElementsByTagName("Message")[0];
            return message.textContent;
        };

        /** getOutputParam */
        soap.getOutputParam = function (request, paramName) {
            var xmlResponse = request.responseXML.documentElement;
            var nodes = xmlResponse.getElementsByTagName("a:KeyValuePairOfstringanyType");
            for (i = 0; i < nodes.length; i++) {
                if (nodes[i].childNodes[0].textContent == paramName) {
                    return nodes[i].childNodes[1].textContent;
                }
            }
            return null;
        };

        /** decorateRequest */
        soap.decorateRequest = function (xmlParamsString, actionName) {
            var requestXML = ""
            requestXML += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
            requestXML += "  <s:Body>";
            requestXML += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
            requestXML += "      <request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">";
            requestXML += "        <a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
            // Request params
            //Ids
            requestXML += xmlParamsString;
            // Action
            requestXML += "        </a:Parameters>";
            requestXML += "        <a:RequestId i:nil=\"true\" />";
            requestXML += "        <a:RequestName>" + actionName + "</a:RequestName>";
            requestXML += "      </request>";
            requestXML += "    </Execute>";
            requestXML += "  </s:Body>";
            requestXML += "</s:Envelope>";
            return requestXML;
        };


        return soap;
    })();


    /** 
     * Form Manager
     *
     * Instead of edit, extend the module like this:
     *   var xrmForm = (function(form){
     *       form.onLoad = function(args) {};
     *       form.publicFunc = function(){};
     *       var privateFunc = function(){};
     *       return form;
     *   })(xrmHelper.formManager || {});
     *
     * @module xrmHelper.formManager
     * @exports formManager
     */
    xrmHelper.formManager = (function () {
        var formManager = {};
        var _formType = {
            create: 1,
            update: 2,
            readOnly: 3,
            disabled: 4,
            quickCreate: 5,
            bulkEdit: 6
        };

        /** Check if form is create */
        formManager.isCreate = function () {
            return Xrm.Page.ui.getFormType() == _formType.create;
        };        

        /** Check if form is read only */
        formManager.isUpdate = function () {
            return Xrm.Page.ui.getFormType() == _formType.update;
        };

        /** Check if form is update */
        formManager.isReadOnly = function () {
            return Xrm.Page.ui.getFormType() == _formType.readOnly;
        };

        /** Check if form is quick create */
        formManager.isQuickCreate = function () {
            return Xrm.Page.ui.getFormType() == _formType.quickCreate;
        };

        /** Check if control exists */
        formManager.existsControl = function (fieldName) {
            if (Xrm.Page.getControl(fieldName) == null) {
                console.warn("Can't find the control: " + fieldName);
                return false;
            } else
                return true;
        };

        /** Check if attribute exists */
        formManager.existsAttribute = function (fieldName) {
            if (Xrm.Page.getAttribute(fieldName) == null) {
                console.warn("Can't find the attribute: " + fieldName);
                return false;
            } else
                return true;
        };

        /** Check if attribute has value */
        formManager.attributeHasValue = function (fieldName) {
            return (formManager.existsAttribute(fieldName) && Xrm.Page.getAttribute(fieldName).getValue() != null);
        };

        /** Set disabled control */
        formManager.setDisabled = function (fieldName, disabled) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.ui.controls.get(fieldName).setDisabled(disabled);
        };

        /** Disable field */
        formManager.disable = function (fieldName) {
            formManager.setDisabled(fieldName, true);
        };

        /** Enable field */
        formManager.enable = function (fieldName) {
            formManager.setDisabled(fieldName, false);
        };

        /** Enable/dsable field */
        formManager.setEnabled = function (fieldName, enabled) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.ui.controls.get(fieldName).setDisabled(!enabled);
        };

        /** Set field as required */
        formManager.setRequired = function (fieldName) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.ui.controls.get(fieldName).setRequiredLevel("required");
        };

        /** Set field as not required */
        formManager.setNotRequired = function (fieldName) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.ui.controls.get(fieldName).setRequiredLevel("none");
        };

        /** Show field */
        formManager.show = function (fieldName) {
            formManager.setVisible(fieldName, true);
        };

        /** Hide field */
        formManager.hide = function (fieldName) {
            formManager.setVisible(fieldName, false);
        };

        /** Hide/show field */
        formManager.setVisible = function (fieldName, visible) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.getControl(fieldName).setVisible(visible);
        };

        /** Get value of generic attribute */
        formManager.getValue = function (fieldName) {
            if (!formManager.existsAttribute(fieldName))
                return null;
            return Xrm.Page.getAttribute(fieldName).getValue();
        };

        /** Set value of generic attribute */
        formManager.setValue = function (fieldName, value, forceDisabled) {
            try {
                if (!formManager.existsAttribute(fieldName))
                    return null;
                if (forceDisabled == true)
                    formManager.setDisabled(fieldName, false);
                var returned = Xrm.Page.getAttribute(fieldName).setValue(value);
                if (forceDisabled == true)
                    formManager.setDisabled(fieldName, true);
                return returned;
            } catch (e) {
                consol.warn("Error setValue(" + fieldName + "," + value + ")");
            }            
        };

        /** Return the value of a lookup field or null */
        formManager.getLookupNameValue = function (fieldName) {
            if (!formManager.attributeHasValue(fieldName))
                return null;
            return Xrm.Page.getAttribute(fieldName).getValue()[0].name;
        };

        /** Return the id of a lookup field or null */
        formManager.getLookupIdValue = function (fieldName) {
            if (!formManager.attributeHasValue(fieldName))
                return null;
            return Xrm.Page.getAttribute(fieldName).getValue()[0].id;
        };

        /** Check if attibute is dirty */
        formManager.isDirty = function (fieldName) {
            if (!formManager.attributeHasValue(fieldName))
                return null;
            return Xrm.Page.getAttribute(fieldName).getIsDirty();
        };

        /** Add a button above a field label */
        formManager.attachButton = function (fieldName, text, iconPath, onClick) {
            if (!formManager.existsControl(fieldName))
                return;
            //Generate button
            var btn = document.createElement("div");
            btn.setAttribute('id', fieldName + '_btn');
            btn.classList.add("ms-crm-Inline-Value");
            btn.style.cursor = "hand";
            btn.style.padding = "3px";
            btn.style.border = "1px solid #ddd";
            btn.style.marginLeft = "0px";
            //btn.style.marginBottom = "2px";
            btn.onmouseover = function () { btn.classList.add("ms-crm-Inline-EditHintState"); }
            btn.onmouseout = function () { btn.classList.remove("ms-crm-Inline-EditHintState"); }
            //prepare icon 16 x 16        
            if (iconPath != null) {
                var icon = document.createElement("img");
                icon.src = iconPath;
                icon.style.width = "16px";
                icon.style.height = "16px";
                icon.style.paddingLeft = "3px";
                icon.style.marginLeft = "3px";
                icon.style.borderLeft = "1px solid #ddd";
                icon.style.cssFloat = "right";
                text = icon.outerHTML + text;
            }
            btn.innerHTML = text;
            btn.onclick = onClick;
            //attach the button (parent.window is because in CRM2016 script are opened in a separate iframe)
            parent.window.document.getElementBy
                (fieldName + "_c").appendChild(btn);
        };

        /** Get etnity id */
        formManager.getEntityId = function () {
            var id = Xrm.Page.data.entity.getId();
            return id.slice(1, -1);
        };

        /** Prevent autosave */
        formManager.preventAutoSave = function () {
            Xrm.Page.data.entity.addOnSave(function (context) {
                var eventArgs = context.getEventArgs();
                if (eventArgs.getSaveMode() == 70)
                    eventArgs.preventDefault();
            });
        };



        // handlers
        /** onLoadCreate */
        formManager.onLoadCreate = function () { };
        /** onLoadUpdate */
        formManager.onLoadUpdate = function () { };
        /** onLoad */
        formManager.onLoad = function () { };
        /** onLoadUpdate */
        formManager.onChangeHandlers = function () { };
        /** onChange */
        formManager.onChange = function (fieldName, callback) {
            if (!formManager.existsControl(fieldName))
                return;
            Xrm.Page.data.entity.attributes.get(fieldName).removeOnChange(callback);
            Xrm.Page.data.entity.attributes.get(fieldName).addOnChange(callback);
        };         
        // To use onAfterSave the field "modifieon" must be present on the form
        //formManager.onChange("modifiedon", formManager.onAfterSave);
        //formManager.onAfterSave = function () {};
        // init
        formManager.init = function (context) {
            formManager.onLoad(arguments);
            switch (Xrm.Page.ui.getFormType()) {
                case _formType.create:
                    formManager.onLoadCreate(arguments);
                    break;
                case _formType.update:
                    formManager.onLoadUpdate(arguments);
                    break;
                case _formType.readOnly:
                    break;
                case _formType.quickCreate:
                    break;
            }
        };
        return formManager;
    })();

    return xrmHelper;
})();
