﻿var myxrm = myxrm || {};
/** 
 * Incident functions
 * 
 * @module myxrm/incident
 * @exports incident
 */
myxrm.incident = (function () {
    var incident = {};
    
    /**
     * If in form then auto init the Form module.
     * 
     * Wait the xrmHelper.formManager is ready
     * and initialize the module.
     */
    var autoInitForm = (function () {
        var moduleName = "incident";
        // Avoid to init form in grid view
        if (Xrm.Page.ui == null)
            return;
        var formManagerReady = setInterval(function () {
            if (typeof eidos.xrmHelper != 'undefined' && eidos.xrmHelper != null) {
                eidos.postel[moduleName].form(eidos.xrmHelper.formManager).init();
                clearInterval(formManagerReady);
                console.log("Module eidos.postel." + moduleName + ".form is ready");
            }
        }, 100);
    })();


    /** 
     * Form functions
     *
     * @module eidos/postel/incident/form
     * @exports form
     */
    incident.form = function (form) {

        /** Define custom public form function */
        form.myPublicFunc = function () {
            //
        };

        /** Define custom private form function  */
        var myPrivateFunc = function () {            
            //        
        };

        /** onLoad handler */
        form.onLoad = function (args) {                        
            form.preventAutoSave(); //cool shortcut
        };

        /** onLoad form create handler*/
        form.onLoadCreate = function () {
            //
        };

        /** onLoad form update handler*/
        form.onLoadUpdate = function () {
            //
        };

        /** onChange handlers */
        form.onChange("field_name", form.myPublicFunc);
        
        /** onAfterSave hack */
        form.onChange("modifiedon", form.myPublicFunc);
        

        /** Sample helper usage  */
        form.sampleHelperUsage = function () {
            // get/set field value
            form.getValue("field_name");
            form.getLookupNameValue("field_name");
            form.getLookupIdValue("field_name");            
            form.setValue("field_name", value); 

            // show/hide field
            form.show("field_name");
            form.hide("field_name");
            form.setVisible("field_name", false);

            // enable/disable field
            form.disble("field_name");
            form.enable("field_name");
            form.setEnabled("field_name", true);
            form.setDisabled("field_name", true);

            // set field as  required/not Required
            form.setRequired("field_name");
            form.setNotRequired("field_name");    

            // check form type
            form.isCreate();
            form.isUpdate();
            form.isReadOnly();
            form.isQuickCreate();    

            // others
            form.getEntityId();
            form.isDirty("field_name");
            xrmHelper.userHasRole("role_name");
        };

        return form;
    };



    /** 
     * Ribbon functions
     * @module myxrm/incident/ribbon
     * @exports ribbon
     */
    incident.ribbon = (function () {
        var ribbon = {};

        /** Keep code organized  
         * myxrm.incident.ribbon.customButtonOne 
         */
        ribbon.customButtonOne = function () {
           //
        };


        /** Keep code organized  
         * myxrm.incident.ribbon.customButtonTwo 
         */
        ribbon.customButtonTwo = function () {
           //
        };



        /** 
         * Enable rules functions
         * formManager is not available here
         * @module myxrm/incident/ribbon/rules
         * @exports rules
         */
        ribbon.rules = (function () {
            var rules = {};
            var statuscode = Xrm.Page.getAttribute("statuscode") != null ? Xrm.Page.getAttribute("statuscode").getValue() : null;
          
            /** Enable rule 
              * myxrm.incident.ribbon.rules.sampleEnableRule 
              */
            rules.sampleEnableRule = function () {
                if (statuscode == 1)
                    return false;
                return true;
            };


            return rules;
        })();

        return ribbon;
    })();



    /** Other custom functions */
    incident.otherCustomFunction = function (currentStatuscode, callback) {
        //
    };


    return incident;
})();